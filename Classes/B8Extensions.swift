//
//  B8Extensions.swift
//  clickTracker
//
//  Created by Pankaj Verma on 31/07/19.
//

import Foundation
import CoreData


// click tracking
extension UIControl {
    static func addClickAnalytics() {
        let originalMethod = class_getInstanceMethod(UIControl.self, #selector(sendAction(_:to:for:)))
        let swizzledMethod = class_getInstanceMethod(UIControl.self, #selector(sendAction1(_:to:for:)))
        if let originalMethod = originalMethod, let swizzledMethod = swizzledMethod {
            method_exchangeImplementations(originalMethod, swizzledMethod)
        }
    }
    
    @objc private func sendAction1(_ action: Selector, to target: Any?, for event: UIEvent?) {
        
        B8ClickTracker.shared.saveAction(action)
        
        //handover the control back
        sendAction1(action, to: target, for: event)
    }
}

extension UIViewController {

    static var topMostViewController:UIViewController {
        var topMostVC: UIViewController = UIApplication.shared.keyWindow!.rootViewController!
        
        //if view controller if presented
        while (topMostVC.presentedViewController != nil) {
            topMostVC = topMostVC.presentedViewController!
        }
        //if view controller is pushed
        if let lastChild =  topMostVC.children.last{
            topMostVC = lastChild
        }
        return topMostVC
    }
}
