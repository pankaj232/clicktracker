#
# Be sure to run `pod lib lint ds.podspec' to ensure this is a
# valid spec before submitting.
#

Pod::Spec.new do |s|
  s.name             = 'clickTracker'
  s.version          = '1.0.1'
  s.summary          = 'Tracking of click events'
  s.description      = 'This pod will automatically collect all the click on UI View and UI Button. No redundant code required in the view controller classes'

  s.homepage         = 'https://bitbucket.org/pankaj232/clicktracker'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
#s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Pankaj Verma' => 'pankajverma232@gmail.com' }
 s.source           = { :git => 'https://bitbucket.org/pankaj232/clicktracker.git', :tag => s.version.to_s }
   s.social_media_url = 'https://ioslead.wordpress.com/'
  s.ios.deployment_target = '10.0'
  s.osx.deployment_target = '10.10'
  s.source_files = 'Classes/*'
  
  #core data
  s.resources = 'Resources/*.xcdatamodeld'
  s.frameworks = 'CoreData'
  
end
