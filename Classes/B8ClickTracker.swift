//
//  ClickTracker.swift
//  clickTracker
//
//  Created by Pankaj Verma on 31/07/19.
//

import Foundation
import UIKit
import CoreData

open class B8ClickTracker {
    
    public static let shared = B8ClickTracker()
    
    private static var screenName:String? = nil
//    private static var clickCount:Int64 = 0
    private init(){}
    
    public func start(){
        UIControl.addClickAnalytics()
    }
    
    
    //MARK:- Core data
    
    /// save click action
    func saveAction(_ action:Selector){
        
        let actionName = "\(action)"
        // uncomment below to ignore the system generated actions
//        if actionName.hasPrefix("_") || actionName.hasPrefix("__")  {
//            return
//        }
        
        let currentScreen = getCurrentScreen()
        //print("currentScreen =  \(currentScreen). \nactionName = \(actionName)")
        
        let context = B8CoreDataStack.sharedStack.mainContext
        let screen = Screen(context: context)
        screen.name = currentScreen
        screen.action = actionName
        
        B8CoreDataStack.sharedStack.saveMainContext()
    }
    
    public func getAllScreens()->[String]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: B8Strings.CoreData.entityName)
        let context = B8CoreDataStack.sharedStack.mainContext
        var allScreens:Set<String> = []
        
        request.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(request)
            if let screens = results as? [Screen]{
                for screen in screens {
                    if let screenName = screen.name  {
                        allScreens.insert(screenName)
                    }
                }
            }
        } catch {
            
            print("Failed")
        }
        return Array(allScreens)
    }
    
    public func getActionsForScreen(_ screenName: String)->[String]{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: B8Strings.CoreData.entityName)
        let context = B8CoreDataStack.sharedStack.mainContext
        request.predicate = NSPredicate(format: "name = %@", screenName)
        var actions:Set<String> = []
        request.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(request)
            if let results = results as? [Screen]{
                
                //remove duplicate
                for result in results  {
                    if let action = result.action  {
                        actions.insert(action)
                    }
                }
            }
            
        } catch {
            
            print("Failed")
        }
        
        return Array(actions)
    }
    
    public func getClicksCountForAction(_ action:String, _ screenName: String)->Int{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: B8Strings.CoreData.entityName)
        let context = B8CoreDataStack.sharedStack.mainContext
        request.predicate = NSPredicate(format: "name = %@ AND action = %@", screenName, action)
        //        var actions:[(name:String, count:Int64)]?
        request.returnsObjectsAsFaults = false
        do {
            let results = try context.fetch(request)
            if let results = results as? [Screen]{
                return results.count
            }
            
        } catch {
            
            print("Failed")
        }
        return 0
    }
    
    private func getCurrentScreen()->String{
        return B8Strings.screenName(vc: UIViewController.topMostViewController)
        
    }
    
}
