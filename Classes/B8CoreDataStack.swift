//
//  CoreDataStack.swift
//  clickTracker
//
//  Created by Pankaj Verma on 31/07/19.
//

import Foundation
import CoreData

class B8CoreDataStack {
    
    static let sharedStack = B8CoreDataStack()
    private init () {}
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        let podBundle = Bundle.init(for: B8CoreDataStack.self)
        let bundleUrl = podBundle.url(forResource: B8Strings.CoreData.dataModelName, withExtension: "momd")
        let managedObjectModel = NSManagedObjectModel(contentsOf: bundleUrl!)
        let container = NSPersistentContainer(name: B8Strings.CoreData.dataModelName, managedObjectModel: managedObjectModel!)
        container.loadPersistentStores(completionHandler: { (description, error) in
            if let error = error {
                print("Unresolved error " + error.localizedDescription)
            }
        })
        return container
    }()
    

    lazy var mainContext: NSManagedObjectContext = {
        return persistentContainer.viewContext
    }()
    
    lazy var secondaryContext: NSManagedObjectContext = {
        let tempContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            tempContext.parent = self.mainContext
        return tempContext
    }()
    
    func saveContext(context:NSManagedObjectContext, completion: (()->Void)? = nil) {
        context.perform {
            self.save(manageObjectContext: context)
            self.saveMainContext(completion: completion)
        }
    }
    
   func saveMainContext(completion: (()->Void)? = nil) -> Void {
            self.mainContext.perform {
                self.save(manageObjectContext: self.mainContext)
                DispatchQueue.main.async {
                    completion?()
                }
            }
    }
    private func save(manageObjectContext:NSManagedObjectContext) {
        guard manageObjectContext.hasChanges else { return }
        do {
            try manageObjectContext.save()
        } catch {
            let nserror = error as NSError
            NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
            abort()
        }
    }
}
