//
//  B8Strings.swift
//  clickTracker
//
//  Created by Pankaj Verma on 01/08/19.
//

import Foundation
import UIKit

enum B8Strings {
    enum CoreData {
        static let dataModelName = "B8"
        static let entityName = "Screen"
    }
    enum  UserDefault{
        static let screenNameKey = "screenName"
        static let unknownScreenName = "UNKNOWN"
    }
    
    static func screenName(vc: UIViewController)->String{
        return "\(type(of: vc))"
    }
}
