//
//  File.swift
//  clickTracker
//
//  Created by Pankaj Verma on 01/08/19.
//

import Foundation

public enum B8Model{
   public struct  Request {}
    
   public struct Response {
        var screenName:String
        var actionName:String
        var count:Int64
    }
}
